﻿using UnityEngine.UI;
using UnityEngine;

public class LevelSelector : MonoBehaviour {

    public FadingController fadingController;
    public Button[] buttons;

    private void Start()
    {
        int levelReached = PlayerPrefs.GetInt("levelReached", 1);
        for (int i = 0; i < buttons.Length; i++)
        {
            if (i + 1 > levelReached)
            {
                buttons[i].interactable = false;
            } 
        }
    }

    public void Select(string levelName)
    {
        fadingController.FadeTo(levelName);
    }
	
}
