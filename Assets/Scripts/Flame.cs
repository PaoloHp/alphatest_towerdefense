﻿
using UnityEngine;

[System.Serializable]
public class Flame {

    public ParticleSystem flames;
    public Transform flameReactor;
}
