﻿using UnityEngine.SceneManagement;
using UnityEngine;

public class MainMenu : MonoBehaviour {

    public string levelToLoad = "MainLevel";
    public FadingController fadingController;

	public void Play()
    {
        fadingController.FadeTo(levelToLoad);
    }

    public void Quit()
    {
        Debug.Log("Quitting...");
        Application.Quit();
    }
}
