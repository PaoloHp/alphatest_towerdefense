﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletMov : MonoBehaviour {

    public Transform target;
    public int damage =50;
    public float speed = 70f;
    public float explosionRadius = 0f;
    public GameObject impactEffect;

    public void seek(Transform _target)
    {
        target = _target;
    }

  


	
	// Update is called once per frame
	void Update () {
        if (target == null)
        {
            Destroy(gameObject);
            return;
        }

        Vector3 dir = target.position - transform.position;
        float currentDistance = speed * Time.deltaTime;

        if(dir.magnitude <= currentDistance)
        {
            hitTarget();
            return;
        }

        transform.Translate(dir.normalized * currentDistance, Space.World);
        transform.LookAt(target);

		
	}

    void hitTarget()
    {
        GameObject effect = Instantiate(impactEffect, transform.position, transform.rotation);
        Destroy(effect, 2f);
        if(explosionRadius > 0f)
        {
            Explode();
        }
        else
        {
            Damage(target);
        }
      
         
        Destroy(gameObject);
    }

    void Explode()
    {
        Collider[] colliders = Physics.OverlapSphere(transform.position, explosionRadius);
        foreach (Collider collider in colliders)
        {
            if (collider.tag == "Enemy")
            {
                Damage(collider.transform);
            }
        }
    }
    
    void Damage(Transform enemy)
    {
        Enemy tmp = enemy.GetComponent<Enemy>();
        if (tmp != null)
        {
            tmp.TakeDamage(damage);
        }
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.green;
        Gizmos.DrawWireSphere(transform.position, explosionRadius);
    }
}
 