﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TurretUI : MonoBehaviour {

    public GameObject ui;
    public Text upgradeCost;
    public Button upgradeButton;
    private Node target;
    public Text sellPrice;
	
    public void SetTarget(Node node)
    {
        this.target = node;
        transform.position = target.GetBuildPosition();
        if (!target.isUpgraded)
        {
            upgradeCost.text = "$" + target.turretBlueprint.upgradeCost;
            upgradeButton.interactable = true;
        }
        else
        {
            upgradeCost.text = "MAX!";
            upgradeButton.interactable = false;
        }
        sellPrice.text = "$" + target.turretBlueprint.GetSellPrice(); 
        ui.SetActive(true);
    }

    public void Hide()
    {
        ui.SetActive(false);
    }

    public void Upgrade()
    {
        target.UpgradeTurret();
        BuildManager.instance.DeselectNode();
    }

    public void Sell()
    {
        target.SellTurret();
        BuildManager.instance.DeselectNode(); 
    }
}
