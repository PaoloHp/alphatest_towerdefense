﻿
using UnityEngine;

public class BuildManager : MonoBehaviour {

    // Singleton
    public static BuildManager instance;   
    private void Awake()
    {
        if (instance != null)
        {
            Debug.LogError("More than one buildManager in scene!");
            return;
        }
        instance = this;
    }
    // Manager 
    private TurretBlueprint turretToBuild;
    private Node selectedNode;
    public TurretUI turretUI;
    public GameObject buildEffect;
    public GameObject sellEffect;
    public GameObject standardTurretPrefab;
    public GameObject missileLuncherPrefab;
    public GameObject laserBeamerPrefab;

    public bool CanBuild { get { return turretToBuild != null; } }
    public bool HasMoney { get { return PlayerStats.Money >= turretToBuild.cost; } }


   public void SelectNode(Node node)
    {
        if(selectedNode == node)
        {
            DeselectNode();
            return;
        }
        selectedNode = node;
        turretToBuild = null;
        turretUI.SetTarget(node);
    }

    public void DeselectNode()
    {
        selectedNode = null;
        turretUI.Hide();
    }

   public void SelectTurretToBuild(TurretBlueprint turret)
    {
        turretToBuild = turret;
        DeselectNode();
    }

    public TurretBlueprint GetTurretToBuild()
    {
        return turretToBuild;
    }


    
}
