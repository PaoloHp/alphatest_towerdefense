﻿
using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Spawner : MonoBehaviour {

    public static int EnemiesAlive = 0;

    public GameManager gameManager;

    public Transform spawnPoint;
    public Wave[] waves;
    public Text timerText;
    public float waveTimer = 10f;
    private float countdown = 2f;
    private int waveNumber=0;

    private void Update()
    {
        if (EnemiesAlive > 0)
        {
            return;
        }
        if (waveNumber == waves.Length)
        {
            gameManager.WinLevel();
            this.enabled = false;
        }
        if (countdown <= 0f)
        {
            StartCoroutine(SpawnWave());
            countdown = waveTimer;
            return;
        }
        countdown -= Time.deltaTime;
        countdown = Mathf.Clamp(countdown, 0f, Mathf.Infinity);
        timerText.text = string.Format("{0:00.00}", countdown);
    }
    IEnumerator SpawnWave()
    {
        Wave wave = waves[waveNumber];
        EnemiesAlive = wave.count;
        for (int i = 0; i < wave.count; i++)
        {
            SpawnEnemy(wave.enemy);
            yield return new WaitForSeconds(1f/wave.rate);
        }
        
        waveNumber++;
        
    }
    void SpawnEnemy(GameObject enemy)
    {
        Instantiate(enemy, spawnPoint.position, spawnPoint.rotation);
        
    }
}
 