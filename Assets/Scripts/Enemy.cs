﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Enemy : MonoBehaviour {

    public float startHealth = 100;
    public float startSpeed = 10f;
    [HideInInspector]
    private float health;
    public float speed;
    public int moneyGain = 50;
    public GameObject deathEffect;
    public Image healthBar;
    private bool isDead = false;

    private void Start()
    {
        speed = startSpeed;
        health = startHealth;
    }


    public void TakeDamage(int dmg)
    {
        health -= dmg;
        healthBar.fillAmount = health / startHealth;
        if (health <= 0)
        {
            Die();
        }
    }

    public void TakeDamage(float dmg)
    {
        int damageTaken = (int)dmg;
        health -= damageTaken;
        healthBar.fillAmount = health / startHealth;
        if (health <= 0 && !isDead)
        {
            Die();
        }
    }

    public void Slow(float factor)
    {
        speed = startSpeed * (1f - factor);
    }

    void Die()
    {
        isDead = true;
        PlayerStats.Money += moneyGain;
        GameObject effect = (GameObject)Instantiate(deathEffect, transform.position, Quaternion.identity);
        Destroy(effect, 5f);
        Spawner.EnemiesAlive--;
        Destroy(gameObject);
    }
}
