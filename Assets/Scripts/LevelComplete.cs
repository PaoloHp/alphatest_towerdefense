﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelComplete : MonoBehaviour {

    public FadingController fadingController;
    public GameManager gameManager;

	 
    public void Continue()
    {
        PlayerPrefs.SetInt("levelReached", gameManager.levelToUnlock);
        fadingController.FadeTo("LevelSelect");
    }
}
