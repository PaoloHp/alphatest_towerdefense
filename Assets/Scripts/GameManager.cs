﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour {
    public static bool GameIsOver;
    public GameObject gameOverUI;
    public GameObject levelCompleteUI;

    
    public int levelToUnlock = 2;
    public FadingController fadingController;
	// Use this for initialization
	void Start ()
    {
        GameIsOver = false;
		
	}
	
	// Update is called once per frame
	void Update ()
    {
        if (GameIsOver)
        {
            return;
        }
        if (Input.GetKeyDown("e"))
        {
            EndGame();
        }
		if(PlayerStats.Lives <= 0)
        {
            EndGame();
        }
	}

    private void EndGame()
    {
        GameIsOver = true;
        gameOverUI.SetActive(true);
    }

    public void WinLevel()
    {
        GameIsOver = true;
        levelCompleteUI.SetActive(true);       
         
    }
}
