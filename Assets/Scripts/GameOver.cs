﻿using UnityEngine.UI;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameOver : MonoBehaviour {

    public FadingController fadingController;

    public void Retry()
    {
        fadingController.FadeTo(SceneManager.GetActiveScene().name);
    }

    public void Menu()
    {
        fadingController.FadeTo("MainMenu");
    }
}
