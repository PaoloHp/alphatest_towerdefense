﻿using System.Collections;
using UnityEngine;
[RequireComponent(typeof(Enemy))]
public class Enemy_AI : MonoBehaviour {

    
    private Transform target;
    private int waypointIndex = 0;
    private Enemy enemy;
    public float rotationSpeed = 10f;
    // TODO - MOVE THIS TO A SEPARATE SCRIPT
    public Flame[] flames;
    private int flameNumber = 0;

    private void Start()
    {
        enemy = GetComponent<Enemy>();
        target = Waypoints.points[0];
        

    }

    private void Update()
    {
        Vector3 dir = target.position - transform.position;
        Quaternion lookRotation = Quaternion.LookRotation(dir);
        Vector3 rotation = Quaternion.Lerp(transform.rotation, lookRotation, Time.deltaTime * rotationSpeed).eulerAngles;
        transform.rotation = Quaternion.Euler(0f, rotation.y, 0f);
        transform.Translate(dir.normalized * enemy.speed * Time.deltaTime, Space.World);

        if (Vector3.Distance(transform.position, target.position) <= 0.4f)
        {
            GetNextWaypoint();
        }
        enemy.speed = enemy.startSpeed;
    }

    // TODO - MOVE THIS TO A SEPARATE SCRIPT
    private void SpawnFlames()
    {
        Flame flame = flames[flameNumber];
        Instantiate(flame.flames, flame.flameReactor.position, Quaternion.identity);

    } 
    void GetNextWaypoint()
    {
        if(waypointIndex >= Waypoints.points.Length - 1)
        {
            EndPath();
            return;
        }
        waypointIndex++;
        target = Waypoints.points[waypointIndex];
    }

    void EndPath()
    {
        PlayerStats.Lives--;
        Spawner.EnemiesAlive--;
        Destroy(gameObject);
    }
}
