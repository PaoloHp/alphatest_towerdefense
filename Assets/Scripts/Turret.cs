﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Turret : MonoBehaviour {

    private Transform target;
    private Enemy targetEnemy;

    [Header("General")]
    public float range = 15f;

    [Header("Use Bullets(default)")]
    public float fireRate = 10f;
    public float fireCountdown = 0f;
    public GameObject bulletPrefab;

    [Header("Use Laser")]
    public bool useLaser = false;
    public float damageOverTime = 100f;
    public float slowFactor = .5f;
    public LineRenderer lineRender;
    public ParticleSystem impactEffect;
    public Light impactLight;

    [Header("Unity Setup Fields")]
    public string enemyTag = "Enemy";
    public Transform rotationPart;
    public float rotationSpeed = 10f;

    public Transform firePoint;
   

    private void Start()
    {
        InvokeRepeating("updateTarget", 0f, 0.5f);
    }

    void updateTarget()
    {
        GameObject[] enemies = GameObject.FindGameObjectsWithTag(enemyTag);

        float shortestDistance = Mathf.Infinity;
        GameObject nearestEnemy = null;
        foreach (GameObject enemy in enemies)
        {
            float distanceToEnemy = Vector3.Distance(transform.position, enemy.transform.position);
            if (distanceToEnemy < shortestDistance)
            {
                shortestDistance = distanceToEnemy;
                nearestEnemy = enemy;

            }
            else
            {
                target = null;
            }
        }
        if (nearestEnemy != null && shortestDistance <= range)
        {
            target = nearestEnemy.transform;
            targetEnemy = nearestEnemy.GetComponent<Enemy>();
        }
    }
    private void Update()
    {
        if (target == null)
        {
            if (useLaser)
            {
                if (lineRender.enabled)
                {
                    lineRender.enabled = false;
                    impactEffect.Stop();
                    impactLight.enabled = false; 
                }
            }
            return;
        }
        LockOn();
        if (useLaser)
        {
            Laser();
        }
        else
        {
            //Shoot
            if (fireCountdown <= 0f)
            {
                shoot();
                fireCountdown = 5f / fireRate;
            }
            fireCountdown -= Time.deltaTime;
        }

    }

    void LockOn()
    {
        Vector3 dir = target.position - transform.position;
        Quaternion lookRotation = Quaternion.LookRotation(dir);
        Vector3 rotation = Quaternion.Lerp(rotationPart.rotation, lookRotation, Time.deltaTime * rotationSpeed).eulerAngles;
        rotationPart.rotation = Quaternion.Euler(0f, rotation.y, 0f);
    }

    void Laser()
    {
        targetEnemy.TakeDamage(damageOverTime * Time.deltaTime);
        targetEnemy.Slow(slowFactor); 
        if (!lineRender.enabled)
        {
            lineRender.enabled = true;
            impactEffect.Play();
            impactLight.enabled = true;
        }
        lineRender.SetPosition(0, firePoint.position);
        lineRender.SetPosition(1, target.position);
        Vector3 dir = firePoint.position - target.position;
        impactEffect.transform.position = target.position + dir.normalized;
        impactEffect.transform.rotation = Quaternion.LookRotation(dir);
        
    }

    void shoot()
    {
        GameObject currentBullet = (GameObject)Instantiate(bulletPrefab, firePoint.position, firePoint.rotation);
        BulletMov bullet = currentBullet.GetComponent<BulletMov>();
        if (bullet != null)
        {
            bullet.seek(target); 
        }
        
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, range);
    }
}
